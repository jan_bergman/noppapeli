/**
 * @author Jan Bergman A Class for abstracting a die.
 */
public class Die {

	private int Dicenumber;

	/**
	 * Throws die and returns the result.
	 * 
	 * @return the result of throwing the die
	 */
	int throwAndgetDicenumber() {
		Dicenumber = 1 + (int) (6 * Math.random());
		return Dicenumber;
	}

	/**
	 * @return the result of throwing the die
	 * 
	 */
	int getDicenumber() {
		return Dicenumber;
	}

}