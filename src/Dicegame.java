import java.util.Scanner;

/**
 * @author Jan Bergman. A class for a Dice game. The player tries to guess the
 *         sum of two dice. The user is notified if the guess is correct.
 *         After that the user can end the game or start a new game. 
 */
public class Dicegame {

	private static Scanner scan;

	/**
	 * The method waits for user input. After user has given input it checks if
	 * the input is integer. Method could be extended to check for a number
	 * between 2-12.
	 * 
	 * @return the number that user has given.
	 */
	private static int userinputInt() {
		int number = 0;

		while (scan.hasNext()) {
			try {
				number = Integer.parseInt(scan.nextLine());
				break;
			} catch (NumberFormatException ex) {
				System.out.println("Et antanut numeroa!");
			}
		}

		return number;
	}

	public static void main(String[] args) {

		Die gameDie = new Die();
		Boolean continuegame = true;
		scan = new Scanner(System.in);

		System.out
				.println("Tervetuloa Noppapeliin!\n" + "Noppapellissä arvaat kahden nopan yhteenlaskettun silmäluvun.");

		while (continuegame) {

			System.out.println("Arvaa silmäluku:");

			// Get user input
			int Guessednumber = userinputInt();

			// Throw die twice and save the result in DiceNumber
			int Dicenumber = gameDie.throwAndgetDicenumber() + gameDie.throwAndgetDicenumber();

			System.out.println("Yhteenlaskettu määrä on " + Dicenumber + ".");

			// Check if user guessed right.
			if (Guessednumber == Dicenumber) {
				System.out.println("Arvasit oikein " + Dicenumber + ".");
			} else {
				System.out.println("Arvasit väärin.");
			}

			// Ask if user wants to play again.
			System.out.println(
					"Haluatko pelata uudelleen? Kirjota kyllä jatkaaksesi tai paina enter lopettaaksesi pelaamisen.");

			// Save the input
			String continueGameAnswer = scan.nextLine();

			// If input is not equal to yes then end game. Here enter is
			// considered input as well.
			if (!continueGameAnswer.equals("kyllä")) {
				continuegame = false;
			}

		}

	}

}
